import ga
import matplotlib.pyplot as plt
from ypstruct import structure


# Sphere Test Functions

def sphere(x):
    return sum(x**2)


def cost_calc(x):
    return sum(x)

# problem Definition


problem = structure()
# problem.cost_func = sphere
problem.cost_func = cost_calc
problem.n_var = 13
problem.var_min = -10
problem.var_max = 10

# GA Parameters
params = structure()
params.max_it = 100
params.n_pop = 5
params.beta = 1
params.pc = 1
params.gamma = 0.1
params.mu = 0.1
params.sigma = 0.1

# Run GA
out = ga.run(problem, params)
print(out.pop)

plt.plot(out.best_cost)
# plt.semilogy(out.best_cost)
plt.xlim(0, params.maxit)
plt.xlabel('Iterations')
plt.ylabel('Best Cost')
plt.title("Genetic Algorithm")
plt.grid(True)
plt.show()
