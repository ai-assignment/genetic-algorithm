
import numpy as np
from ypstruct import structure


def run(problem, params):
    # Problem Information
    cost_func = problem.cost_func
    n_var = problem.n_var
    var_min = problem.var_min
    var_max = problem.var_max

    # Parameters
    max_it = params.max_it
    n_pop = params.n_pop
    beta = params.beta
    pc = params.pc
    nc = int(np.round(pc*n_pop/2)*2)
    gamma = params.gamma
    mu = params.mu
    sigma = params.sigma

    # Empty Individual Template
    empty_individual = structure()
    empty_individual.position = None
    empty_individual.cost = None

    # BestSolution Ever found
    best_sol = empty_individual.deepcopy()
    best_sol.cost = np.inf

    # Initialize population
    pop = empty_individual.repeat(n_pop)

    for i in range(0, n_pop):
        # pop[i].position = np.random.uniform(var_min, var_max, n_var)
        pop[i].position = np.random.randint(2, size=13)
        print(pop[i])
        pop[i].cost = cost_func(pop[i].position)
        if pop[i].cost < best_sol.cost:
            best_sol = pop[i].deepcopy()

    # Best of iterations
    best_cost = np.empty(max_it)

    # Main Loop
    for it in range(max_it):

        # Selection probabilities
        costs = np.array([x.cost for x in pop])
        avg_cost = np.mean(costs)
        if avg_cost != 0:
            costs = costs/avg_cost
        probs = np.exp(-beta*costs)

        # children population
        popc = []
        for k in range(nc//2):

            # select parents
            # q = np.random.permutation(n_pop)
            # p1 = pop[q[0]]
            # p2 = pop[q[1]]

            # Perform Roulette wheel selction
            p1 = pop[roulette_wheel_selection(probs)]
            p2 = pop[roulette_wheel_selection(probs)]

            # perform crossover
            # c1, c2 = crossover(p1, p2, gamma)
            c1, c2 = single_point_crossover(p1,p2, n_var)

            # perform mutation
            # c1 = mutate(c1, mu, sigma)
            # c2 = mutate(c2, mu, sigma)
            c1 = binary_mutation(c1, mu)
            c2 = binary_mutation(c2, mu)

            # Apply bounds
            apply_bound(c1, var_min, var_max)
            apply_bound(c2, var_min, var_max)

            # Evaluate first offsprings
            c1.cost = cost_func(c1.position)
            if c1.cost < best_sol.cost:
                best_sol = c1.deepcopy()

            # Evaluate second offsprings
            c2.cost = cost_func(c2.position)
            if c2.cost < best_sol.cost:
                best_sol = c2.deepcopy()

        # Add offsprings to popc
        popc.append(c1)
        popc.append(c2)

        # Merge, sort and select
        pop += popc
        pop = sorted(pop, key=lambda x: x.cost)
        pop = pop[0:n_pop]

        # store best cost or elitism
        best_cost[it] = best_sol.cost

        # Show iteration information
        print("Iteration {}: Best Cost = {}".format(it, best_cost[it]))

    # Output
    out = structure()
    out.pop = pop
    out.best_sol = best_sol
    out.best_cost = best_cost
    return out

# single point crossover


def single_point_crossover(p1, p2, n_var):
    c1 = p1.deepcopy()
    c2 = p1.deepcopy()

    j_index = np.random.randint(1, n_var - 1)
    c1.position = np.concatenate((p1.position[:j_index], p2.position[j_index:]), axis=None)
    c2.position = np.concatenate((p2.position[:j_index], p1.position[j_index:]), axis=None)

    return c1, c2

# double point crossover


def double_point_crossover():
    return None

# using uniform crossover


def crossover(p1, p2, gamma):
    c1 = p1.deepcopy()
    c2 = p1.deepcopy()
    # uniform crossover by using numpy array to multiply 2 vectors by normal
    # multiplication. initially generate random alpha
    alpha = np.random.uniform(-gamma, 1+gamma, *c1.position.shape)
    print(alpha)
    c1.position = alpha*p1.position + (1-alpha)*p2.position
    c2.position = alpha*p2.position + (1-alpha)*p1.position
    return c1, c2

# will use binary mutation in diabetic feature selection


def binary_mutation(c, mu):
    ind = np.argwhere(np.random.rand(*c.position.shape) <= mu)
    cm = c.deepcopy()
    cm.position[ind] = 1 - c.position[ind]
    return cm


def mutate(x, mu, sigma):
    y = x.deepcopy()
    # flag = np.random.rand(*x.position.shape) <= mu
    ind = np.argwhere(np.random.rand(*x.position.shape) <= mu)
    y.position[ind] += sigma*np.random.randn(*ind.shape)
    return y


def apply_bound(x, var_min, var_max):
    x.position = np.maximum(x.position, var_min)
    x.position = np.minimum(x.position, var_max)


def roulette_wheel_selection(p):
    c = np.cumsum(p)
    r = sum(p)*np.random.rand()
    ind = np.argwhere(r <= c)
    return ind[0][0]